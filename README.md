Champion Scripts
------------
*   \[VIP\] Blitzcrank - [BOL thread](http://botoflegends.com/forum/topic/11200-blitzcrank-grab-them-all/).
*   \[VIP\] Cassiopeia - [BOL thread](http://botoflegends.com/forum/topic/11271-standalone-cassiopeia/).
*   \[VIP\] Kogmaw - [BOL thread](http://botoflegends.com/forum/topic/10513-standalone-ap-kogmaw-prodiction/).
*   \[VIP\] Twisted Fate - [BOL thread](http://botoflegends.com/forum/topic/10891-twisted-fate-standalone/).
*   \[VIP\] Veigar - [BOL thread](http://botoflegends.com/forum/topic/10387-proveigar-10-standalone/).
*   \[VIP\] Shaco - [BOL thread](http://botoflegends.com/forum/topic/11920-advanced-shaco/).
*   \[VIP\] Orianna - [BOL thread](http://botoflegends.com/forum/topic/12457-standalone-orianna/).

Utility Scripts:
------------
*   \[Free\] ProTracker - [BOL thread](http://botoflegends.com/forum/topic/10541-protracker-cd-tracker/).
*   \[VIP\] Queuer - [BOL thread](http://botoflegends.com/forum/topic/10590-action-queuer-nidalee-jumper-and-more/).
*   \[VIP\] R4Me - [BOL thread](http://botoflegends.com/forum/topic/11412-r4me-ultimate-helper/).
*   \[VIP\] ShoppingBasket - [BOL thread](http://botoflegends.com/forum/topic/12019-my-shopping-basket/).

Libs:
------------
*   \[VIP\] VPrediction - [BOL thread](http://botoflegends.com/forum/topic/11076-library-vip-vprediction/).
